var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var bodyParser = require('body-parser');
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));

app.use(bodyParser.urlencoded({extended:true})); // for parsing application/json
app.use(bodyParser.json());

let users = [];

app.get('/', function(req, res){
  // res.sendFile(__dirname + '/index.html');
  res.sendFile(__dirname + '/deuxdex.html');
});


app.post('/register', function(req, res) {
  console.log("Registration request ["+JSON.stringify(req.body)+"]");
  if (req.body.username.length > 0 && req.body.password.length > 0) {
    users.push({username:req.body.username,password:req.body.password, token:undefined});
  }
});

app.post('/auth', function(req, res,next) {
  console.log("Authentication request ["+JSON.stringify(req.body)+"]");
  let sent = false;
  for (let user of users) {
    console.log("User checked ["+JSON.stringify(user)+"]");
    if (user.username == req.body.username && user.password == req.body.password) {
      user.token = '_'+Math.random().toString()+'_'+Math.random().toString();
      console.log("FOund user ["+JSON.stringify(user)+"]");
      res.send({"token":user.token});
      sent = true;
    }
  }
  if (sent == false) {
    res.sendStatus(401);
  }
});

let checkUserToken = (token) => {
  for (let user of users) {
    if (user.token == token) {
      return true;
    }
  }
  return false;
};

let getUserByToken = (token) => {
  for (let user of users) {
    if (user.token == token) {
      return user;
    }
  }
  return false;
};

let getMessages = () => {};

let appendMessage = () => {};

io.on('connection', function(socket){
  socket.broadcast.emit('hi');
  console.log('a user connected');
  socket.on('disconnect', function(){
    // console.log("User msg disconnect ["+JSON.stringify(msg)+"]");
    // let user = getUserByToken(msg.token);
    // user.token = undefined;
    console.log('user disconnected');
  });
  socket.on('chat message', function(msg){
    let user = getUserByToken(msg.token);
    if (msg.msg == "/disconnect") {
      if (user) {
        user.token = undefined;
        io.emit('chat message', {username:user.username, msg:"User disconnected."});
      }
    } else {
      if (user) {
        console.log('message: ' + msg.msg);
        io.emit('chat message', {username:user.username, msg:msg.msg});
      }
    }
  });
});

io.emit('some event', { someProperty: 'some value', otherProperty: 'other value' }); // This will emit the event to all connected sockets

http.listen(3000, function(){
  console.log('listening on *:3000');
});
